<?php

include 'model/CreateItem.php';

/**
 * Funcion que actualiza el item
 * Primero verifica si es una consulta POST y si el ID no esta vacio
 * Luego llama a la funcion update de la clase CreateItem
 * 
 * Luego veifica si es una consulta GET y si el ID no esta vacio
 * Luego llama a la funcion view de la clase CreateItem
 * 
 * @param int $id
 * @param string $producto
 * @param double $precio
 * @param bool $status
 * @throws Throwable
 * 
 * 
 */
try {
    if ($_SERVER["REQUEST_METHOD"] === "GET") {
        $viewItems = new CreateItem();
        $resultado = $viewItems->handleView($_GET["id"]);
        echo $resultado;
    }

    
    if ($_SERVER["REQUEST_METHOD"] === "POST") {
        $deleteItem = new CreateItem();
        $resultado = $deleteItem->handleUpdate($_POST["id"], $_POST["producto"], $_POST["precio"], $_POST["status"]);
        echo $resultado;
    }
    
} catch (\Throwable $th) {
    echo json_encode(["error" => $th->getMessage()]);
}