<?php

include 'model/CreateItem.php';

try {
    if ($_SERVER["REQUEST_METHOD"] === "POST") {
        $deleteItem = new CreateItem();
        $resultado = $deleteItem->handleCreate($_POST["producto"], $_POST["precio"]);
        echo $resultado;
    }
} catch (\Throwable $th) {
    echo json_encode(["error" => $th->getMessage()]);
}
