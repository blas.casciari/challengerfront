<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Listado</title>
</head>

<body>

    <div class="container mt-4">
        <h2>Listado de Productos</h2>
        <button class="btn btn-success" id="btnCreate">Nuevo Producto</button>
        <button class="btn btn-dark" id="createTableProduct">Crear Tabla</button>
        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Producto</th>
                    <th>Precio ARS</th>
                    <th>Precio USD</th>
                    <th>Estado</th>
                    <th>Acción</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>

    <div id="modalProduct" class="modal">
        <div class="modal-contenido">
            <span class="cerrar-modal" id="btnCerrarModal">&times;</span>
            <h2>Formulario Modal</h2>
            <form id="formProduct">
                <input type="hidden" name="id" id="id">

                <label for="producto">Producto:</label>
                <input type="text" id="producto" name="producto">

                <label for="precio">Precio:</label>
                <input type="text" id="precio" name="precio" pattern="[0-9]+([\.,][0-9]+)?">

                <label for="status">Estado:</label>
                <select name="status" id="status">
                    <option value="1">Activo</option>
                    <option value="0">Inactivo</option>
                </select>

                <input type="submit" id="btnEnviar" value="cargar">
            </form>
        </div>
    </div>

    <div id="modalProductCreate" class="modal">
        <div class="modal-contenido">
            <span class="cerrar-modal" id="btnCerrarModal">&times;</span>
            <h2>Crear</h2>
            <form id="formProductCreate">

                <label for="nuevoProducto">Producto:</label>
                <input type="text" id="nuevoProducto" name="nuevoProducto">

                <label for="nuevoPrecio">Precio:</label>
                <input type="text" id="nuevoPrecio" name="nuevoPrecio" pattern="[0-9]+([\.,][0-9]+)?">

                <input type="submit" id="btnEnviarCreate" value="cargar">
            </form>
        </div>
    </div>

    <!-- Incluye la biblioteca de Bootstrap JS si es necesario -->

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.7.1.min.js" integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
</body>

<link rel="stylesheet" href="css/index.css">

</html>

<script src="js/product.js"></script>