<?php

include 'model/CreateItem.php';

/**
 * Funcion que elimina el item
 * Primero verifica si es una consulta POST y si el ID no esta vacio
 * Luego llama a la funcion delete de la clase CreateItem
 * @param int $id
 * @throws Throwable
 * 
 */
try {
    if ($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST["id"])) {
        $deleteItem = new CreateItem();
        $id = $_POST["id"];
        $resultado = $deleteItem->handleDelete($id);
        echo true;
    }
} catch (\Throwable $th) {
    echo json_encode(["error" => $th->getMessage()]);
}
