<?php

include 'model/CreateItem.php';

/**
 * Funcion que crea la tabla en la base de datos
 * 
 * Si se ejecuta mediante el metodo POST crea la tabla.
 * Si se ejecuta mediante el metodo GET verifica si la tabla existe.
 * @throws Throwable
 */

try {
    if ($_SERVER["REQUEST_METHOD"] === "POST") {
        $deleteItem = new CreateItem();
        $resultado = $deleteItem->table();
        echo $resultado;
    }
    if ($_SERVER["REQUEST_METHOD"] === "GET") {
        $deleteItem = new CreateItem();
        $resultado = $deleteItem->tablaExiste();
        echo $resultado;
    }
} catch (\Throwable $th) {
    echo json_encode(["error" => $th->getMessage()]);
}
