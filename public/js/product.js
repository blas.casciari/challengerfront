class actionProdcut {

    /**
     * Esta funcion se encarga de borrar un registro a traves de una peticion ajax
     * @param {*} id 
     */
    static delete(id) {
        $.ajax({
            type: "POST",
            url: "Delete.php",
            data: {
                id: id
            },
            dataType: "JSON",
            success: function (response) {
                actionProdcut.view();
            }
        });
    }

    /**
     * Esta funcion se encarga de obtener los valores de la base de datos. Para posteriormente mostrarlos en los input del modal
     * 
     *  @param {*} id
     *  @param {*} nombre
     *  @param {*} precio
     *  @param {*} estado
     * */
    static viewItemUpdate(id) {
        $.ajax({
            type: "GET",
            url: "Update.php",
            data: {
                id: id
            },
            dataType: "JSON",
            success: function (response) {
                var responseObject = response[0];
                var nombre = responseObject.nombre;
                var precioARS = responseObject.precio_ARS;
                var estado = responseObject.estado;
                var id = responseObject.id;

                $("#producto").val(nombre);
                $("#precio").val(precioARS);
                $("#status").val(estado);
                $("#id").val(id);
            },
            error: function (xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }

    /**
     * Esta funcion se encarga de obtener los valores de la base de datos. Para posteriormente mostrarlos en la tabla
     * 
     * */
    static view() {
        $.ajax({
            type: "GET",
            url: "view.php",
            dataType: "JSON",
            success: function (response) {
                actionProdcut.llenarTabla(response);
            }
        });
    }

    /**
     * Esta funcion se encarga de mostrarlos en la tabla de la pagina index
     * 
     * @param {*} data
     * @returns
     * */
    static llenarTabla(data) {
        var tabla = $("tbody");
        tabla.empty();

        $.each(data, function (index, registro) {
            var estadoTexto = (registro.estado === 1) ? "Activo" : "Inactivo";

            var fila = "<tr>" +
                "<td>" + registro.id + "</td>" +
                "<td>" + registro.nombre + "</td>" +
                "<td>" + registro.precio_ARS + "</td>" +
                "<td>" + registro.precio_USD + "</td>" +
                "<td>" + estadoTexto + "</td>" +
                "<td>" +
                "<button class='btn btn-danger' id='" + registro.id + "'>Eliminar</button>" +
                "<button class='btn btn-primary' id='" + registro.id + "'>Editar</button>" +
                "</td>" +
                "</tr>";

            tabla.append(fila);
        });
    }

    /**
     * Esta funcion se encarga, con la informacion obtenida, crear un registro en la Base de Datos
     * @param {*} producto 
     * @param {*} precio 
     */
    static create(producto, precio) {
        $.ajax({
            type: "POST",
            url: "Create.php",
            data: {
                producto: producto,
                precio: precio
            },
            dataType: "JSON",
            success: function (response) {
                $("#modalProductCreate").fadeOut();
                $("#nuevoProducto").val('');
                $("#nuevoPrecio").val('');
                actionProdcut.view();
            },
            error: function (xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }

    /**
     * Esta funcion se encarga de verificar si una tabla existe en la base de datos
     * 
     * */
    static verifyTable() {
        $.ajax({
            type: "GET",
            url: "migration.php",
            dataType: "JSON",
            success: function (response) {
                if (response) {
                    $('#createTableProduct').remove();
                }
            }
        });
    }
}


$(document).ready(function () {
    actionProdcut.view();
    actionProdcut.verifyTable();

    /**
     * Maneja el clic en botones con la clase ".btn-danger".
     * Al hacer clic, evita el comportamiento predeterminado del enlace, obtiene el ID del botón
     * y luego invoca la función de delete de la clase actionproduct con ese ID.
     *
     * @param {Event} e - El objeto del evento de clic.
     * @returns {void}
     */
    $(document).on("click", ".btn-danger", function (e) {
        e.preventDefault();
        let id = $(this).attr("id");

        actionProdcut.delete(id);
    });

    /**
     * Maneja el clic en botones con la clase ".btn-primary".
     * Al hacer clic, evita el comportamiento predeterminado del enlace, obtiene el ID del botón
     * y luego invoca la función de viewItemUpdate de la clase actionproduct con ese ID.
     *
     * @param {Event} e - El objeto del evento de clic.
     * @returns {void}
     */
    $(document).on("click", ".btn-primary", function (e) {
        let id = $(this).attr("id");

        actionProdcut.viewItemUpdate(id);

        $("#modalProduct").fadeIn();
    });

    /**
     * Maneja el clic en botones con el id "btnCerrarModal".
     * Esta funcion se encarga de cerrar el modal y de ocultar el contenido del modal.
     */
    $("#btnCerrarModal, .modal").click(function () {
        $("#modalProduct").fadeOut();
        $("#modalProductCreate").fadeOut();
    });
    
    /**
     * Evitar que el clic en el contenido del modal cierre el modal
     */
    $(".modal-contenido").click(function (event) {
        event.stopPropagation();
    });

    /**
     * 
     * Maneja el evento submit del formulario de actualización.
     * Al hacer clic, evita el comportamiento predeterminado del formulario, obtiene los valores de los campos del formulario
     * 
     * @param {Event} e - El objeto del evento de clic.
     * @returns {void}
     * 
     * */
    $("#formProduct").submit(function (event) {
        event.preventDefault();

        var producto = $("#producto").val();
        var precio = $("#precio").val();
        var status = $("#status").val();
        var id = $("#id").val();

        $.ajax({
            type: "POST",
            url: "Update.php",
            data: {
                id: id,
                producto: producto,
                precio: precio,
                status: status
            },
            dataType: "JSON",
            success: function (response) {
                $("#modalProduct").fadeOut();
                actionProdcut.view();
            }
        });

        $("#modalProduct").fadeIn();
    });

    /**
     * Maneja el evento click del botón "btnCreate".
     * Al hacer clic, evita el comportamiento predeterminado del botón, muestra el modal de creación.
     * 
     * */
    $(document).on("click", "#btnCreate", function (e) {
        $("#modalProductCreate").fadeIn();
    });

    /**
     * Maneja el evento submit del formulario de creación.
     * Al hacer clic, evita el comportamiento predeterminado del formulario, obtiene los valores de los campos del formulario.
     * Luego invoca la función create de la clase actionproduct con los valores de los campos.
     * 
     * @param {Event} e - El objeto del evento de clic.
     * @returns {void}
     * */
    $("#formProductCreate").submit(function (e) {
        e.preventDefault();

        var producto = $("#nuevoProducto").val();
        var precio = $("#nuevoPrecio").val();

        actionProdcut.create(producto, precio);

        $("#modalProductCreate").fadeIn();

    });

    /**
     * Crea la tabla de migración de la base de datos.
     * Al hacer clic, invoca la función create de la clase actionproduct.
     * 
     * @returns {void}
     * 
     **/
    $('#createTableProduct').click(function () {
        $.ajax({
            type: "POST",
            url: "migration.php",
            dataType: "JSON",
            success: function (response) {
                $('#createTableProduct').remove();
            }
        });
    })
});