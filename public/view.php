<?php

include 'model/CreateItem.php';

/**
 * Funcion que crea la vista
 * 
 * Si se ejecuta mediante el metodo POST crea la vista.
 */
try {
    if ($_SERVER["REQUEST_METHOD"] === "GET") {
        $deleteItem = new CreateItem();
        $resultado = $deleteItem->handleView();
        echo $resultado;
    }
} catch (\Throwable $th) {
    echo json_encode(["error" => $th->getMessage()]);
}